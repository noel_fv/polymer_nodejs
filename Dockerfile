FROM node:latest


WORKDIR /app

#copio archivos
ADD /build/default /app/build/default
ADD server.js /app
ADD package.json /app

#dependencies
RUN npm install

#puerto que expongo
EXPOSE 3000

#comando
CMD ["npm","start"]
